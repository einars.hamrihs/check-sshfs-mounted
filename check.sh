#!/usr/bin/env bash
cd "$(dirname "$0")";
source config.sh

for i in ${!paths[@]};
do
  path=${paths[$i]}
  if (! mountpoint -q "$path" ); then
    echo "${path} is not a mountpoint"
    sshfs -o nonempty,idmap=user ${DEST_HOST}:${remote_paths[$path]} ${path}
  else
    echo "${path} is a mountpoint"
  fi
done
