# check-sshfs-mounted
Checks if sshfs link is established and mounts it, if it doesn't.


## Getting started

- Add paths and host to check in config.sh
- Add to crontab delete_old_backups.sh script - e.g.:
```
*/5 * * * * /path/to/project/check-sshfs-mounted/check.sh
```
